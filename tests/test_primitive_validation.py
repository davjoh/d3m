import typing
import unittest
import logging

from d3m import container, exceptions
from d3m.metadata import hyperparams, base as metadata_base
from d3m.primitive_interfaces import base, transformer

Inputs = container.List
Outputs = container.List


class Hyperparams(hyperparams.Hyperparams):
    pass


class TestPrimitiveValidation(unittest.TestCase):
    def test_multi_produce_missing_argument(self):
        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, '\'multi_produce\' method arguments have to be an union of all arguments of all produce methods, but it does not accept all expected arguments'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                })

                def produce(self, *, inputs: Inputs, second_inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

    def test_multi_produce_extra_argument(self):
        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, '\'multi_produce\' method arguments have to be an union of all arguments of all produce methods, but it accepts unexpected arguments'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                })

                def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

                def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, second_inputs: Inputs, timeout: float = None, iterations: int = None) -> base.MultiCallResult:
                    pass

    def test_produce_using_produce_methods(self):
        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, 'Produce method cannot use \'produce_methods\' argument'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                })

                def produce(self, *, inputs: Inputs, produce_methods: typing.Sequence[str], timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

    def test_hyperparams_to_tune(self):
        with self.assertRaisesRegex(exceptions.InvalidMetadataError, 'Hyper-parameter in \'hyperparams_to_tune\' metadata does not exist'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                    'hyperparams_to_tune': [
                        'foobar',
                    ]
                })

                def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

    def test_inputs_across_samples(self):
        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, 'Method \'.*\' has an argument \'.*\' set as computing across samples, but it does not exist'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                    'hyperparams_to_tune': [
                        'foobar',
                    ]
                })

                @base.inputs_across_samples('foobar')
                def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, 'Method \'.*\' has an argument \'.*\' set as computing across samples, but it is not a PIPELINE argument'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                    'hyperparams_to_tune': [
                        'foobar',
                    ]
                })

                @base.inputs_across_samples('timeout')
                def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

    # TODO: Currently only warning messages are generated.
    #       When the compliance goes live after January 2019 verify that the correct exceptions are being thrown.
    def test_can_detect_too_many_package_components(self):
        logger = logging.getLogger('d3m.metadata.base')

        # Ensure a warning message is generated for too many package components
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.random_forest.SKLearn.toomany', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].msg,
                         "Primitive's Python path (%(python_path)s) does not adhere to d3m.primitives namespace specification "
                         "(see https://gitlab.com/datadrivendiscovery/d3m/issues/3). Reason: must have 5 segments. This API will "
                         "be made mandatory after January 2019 and this warning will become an exception.")

        # Ensure a warning message is NOT generated for an acceptable number of components
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            logger.debug("Dummy log")
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.random_forest.SKLearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)

    # TODO: Currently only warning messages are generated.
    #       When the compliance goes live after January 2019 verify that the correct exceptions are being thrown.
    def test_can_detect_too_few_package_components(self):
        logger = logging.getLogger(metadata_base.__name__)

        # Ensure a warning message is generated for too few package components
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.too_few', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].msg,
                         "Primitive's Python path (%(python_path)s) does not adhere to d3m.primitives namespace specification "
                         "(see https://gitlab.com/datadrivendiscovery/d3m/issues/3). Reason: must have 5 segments. This API will "
                         "be made mandatory after January 2019 and this warning will become an exception.")

        # Ensure a warning message is NOT generated for an acceptable number of components
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            logger.debug("Dummy log")
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.random_forest.SKLearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)

    # TODO: Currently only warning messages are generated.
    #       When the compliance goes live after January 2019 verify that the correct exceptions are being thrown.
    def test_can_detect_bad_primitive_family(self):
        logger = logging.getLogger(metadata_base.__name__)

        # Ensure a warning message is generated for a bad primitive family
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.bad_family.random_forest.SKLearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].msg,
                         "Primitive's Python path (%(python_path)s) does not adhere to d3m.primitives namespace specification "
                         "(see https://gitlab.com/datadrivendiscovery/d3m/issues/3). Reason: primitive family segment must match primitive's primitive family. This API will "
                         "be made mandatory after January 2019 and this warning will become an exception.")

        # Ensure a warning message is NOT generated for an acceptable primitive family
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            logger.debug("Dummy log")
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.random_forest.SKLearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)

    # TODO: Currently only warning messages are generated.
    #       When the compliance goes live after January 2019 verify that the correct exceptions are being thrown.
    def test_can_detect_bad_primitive_name(self):
        logger = logging.getLogger(metadata_base.__name__)

        # Ensure a warning message is generated for a bad primitive name
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.bad_name.SKLearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].msg,
                         "Primitive's Python path (%(python_path)s) does not adhere to d3m.primitives namespace specification "
                         "(see https://gitlab.com/datadrivendiscovery/d3m/issues/3). Reason: must have a known primitive name segment.")

        # Ensure a warning message is NOT generated for an acceptable primitive name
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            logger.debug("Dummy log")
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.random_forest.SKLearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)

    # TODO: Currently only warning messages are generated.
    #       When the compliance goes live after January 2019 verify that the correct exceptions are being thrown.
    def test_can_detect_kind_not_capitalized(self):
        logger = logging.getLogger(metadata_base.__name__)

        # Ensure a warning message is generated for a primitive kind not capitalized properly
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.random_forest.sklearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].msg,
                         "Primitive's Python path (%(python_path)s) does not adhere to d3m.primitives namespace specification "
                         "(see https://gitlab.com/datadrivendiscovery/d3m/issues/3). Reason: primitive kind segment must start with upper case. This API will "
                         "be made mandatory after January 2019 and this warning will become an exception.")

        # Ensure a warning message is NOT generated for an acceptable primitive kind
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            logger.debug("Dummy log")
            metadata_base.PrimitiveMetadata()._validate_namespace_compliance('d3m.primitives.classification.random_forest.SKLearn', metadata_base.PrimitiveFamily.CLASSIFICATION)

        self.assertEqual(len(cm.records), 1)


if __name__ == '__main__':
    unittest.main()
