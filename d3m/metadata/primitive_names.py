# Primitive Python paths (Python paths under which primitives registers themselves) have to adhere to namespace rules.
# Those rules describe that the Python path consists of multiple segments, one of them being "primitive name". Those
# names should be a general name to describe the logic of a primitive with the idea that multiple implementations
# of the same logic share the same name. This file contains a list of known and allowed primitive names.
# Names should be descriptive and something which can help people understand what the primitive is about.
# You can assume general understanding of data science concepts and names.
#
# Everyone is encouraged to help currate this list and suggest improvements (merging, removals, additions)
# of values in that list by submitting a merge request. We are not strict about names here, the main purpose of
# this list is to encourage collaboration and primitive name reuse when that is reasonable. Please check the list
# first when deciding on a Python path of your primitive and see if it can fit well under an existing name.
#
# See: https://gitlab.com/datadrivendiscovery/d3m/issues/3

PRIMITIVE_NAMES = [
    'add_semantic_types',
    'adjacency_spectral_embedding',
    'ape',
    'arima',
    'astmbplus_selector',
    'audio_featurization',
    'audio_reader',
    'audio_slicer',
    'bayesian_logistic_regression',
    'cast_to_type',
    'channel_averager',
    'cleaning_featurizer',
    'cluster',
    'cluster_curve_fitting_kmeans',
    'collaborative_filtering_link_prediction',
    'collaborative_filtering_parser',
    'column_parser',
    'community_detection',
    'community_detection_parser',
    'computes_cores',
    'conditioner',
    'construct_predictions',
    'convolutional_neural_net',
    'corex_continuous',
    'corex_supervised',
    'corex_text',
    'cover_tree',
    'croc',
    'csv_reader',
    'cut_audio',
    'data_cleaning',
    'data_frame_to_list',
    'data_frame_to_ndarray',
    'data_frame_to_tensor',
    'data_set_text_reader',
    'data_set_to_data_frame',
    'deep_markov_bernoulli_forecaster',
    'deep_markov_categorical_forecaster',
    'deep_markov_gaussian_forecaster',
    'denormalize',
    'diagonal_mvn',
    'dimension_selection',
    'doc_2_vec',
    'edge_list_to_graph',
    'ekss',
    'encoder',
    'extract_columns',
    'extract_columns_by_semantic_types',
    'extract_columns_by_structural_types',
    'f_astmb_selector',
    'f_sstmb_selector',
    'f_stmb_selector',
    'fast_lad',
    'feed_forward_neural_net',
    'forward',
    'gaussian_classification',
    'gaussian_clustering',
    'general_relational_dataset',
    'glda',
    'glis',
    'gmm',
    'go_dec',
    'goturn',
    'graph_matching_link_prediction',
    'graph_matching_parser',
    'graph_node_splitter',
    'graph_to_edge_list',
    'graph_transformer',
    'grasta',
    'grasta_masked',
    'greedy_imputation',
    'grouse',
    'hdp',
    'horizontal_concat',
    'i3d',
    'ibex',
    'image_reader',
    'image_transfer_learning_transformer',
    'increment_primitive',
    'ipcmb_plus_selector',
    'iqr_scaler',
    'iterative_regression_imputation',
    'i_vector_extractor',
    'jmi_plus_selector',
    'kfold_dataset_split',
    'kmeans',
    'kss',
    'l1_low_rank',
    'labler',
    'laplacian_spectral_embedding',
    'largest_connected_component',
    'lda',
    'learner',
    'linear_regression',
    'link_prediction',
    'listto_data_frame',
    'list_to_ndarray',
    'log_mel_spectrogram',
    'loss',
    'lupi_svm',
    'mean_baseline',
    'mean_imputation',
    'meta_feature_extractor',
    'monomial_primitive',
    'multitable_featurization',
    'ndarray_to_dataframe',
    'ndarray_to_list',
    'nk_sent2vec',
    'non_parametric_clustering',
    'number_of_clusters',
    'one_hot_maker',
    'out_of_core_adjacency_spectral_embedding',
    'owl_regression',
    'pass_to_ranks',
    'pca',
    'pca_features',
    'pcp_ialm',
    'profiler',
    'pyglrm_d3huber_pca',
    'pyglrm_d3low_rank_imputer',
    'random_forest',
    'random_primitive',
    'random_projection_timeseries_featurization',
    'random_sampling_imputer',
    'redact_targets',
    'relationalt_imeseries',
    'remove_columns',
    'remove_duplicate_columns',
    'remove_semantic_types',
    'replace_semantic_types',
    'resnet50_image_feature',
    'reverse',
    'rfd',
    'rffeatures',
    'rfm_precondition_ed_gaussian_krr',
    'rfm_precondition_ed_polynomial_krr',
    'rpca_lbd',
    'search',
    'search_hybrid',
    'search_hybrid_numeric',
    'search_numeric',
    'seeded_graph_matching',
    'segment_curve_fitter',
    'sequence_to_bag_of_tokens',
    'signal_dither',
    'signal_framer',
    'signal_mfcc',
    'simon',
    'spectral_graph_clustering',
    'ssc_admm',
    'ssc_cvx',
    'ssc_omp',
    'sstmb_plus_selector',
    'stacking_operator',
    'stack_ndarray_column',
    'stmb_plus_selector',
    'sum_primitive',
    'targets_reader',
    'tensor_machines_binary_classification',
    'tensor_machines_regularized_least_squares',
    'text_reader',
    'time_series_to_list',
    'train_score_dataset_split',
    'trecs',
    'unary_encoder',
    'unicorn',
    'uniform_segmentation',
    'unseen_label_decoder',
    'unseen_label_encoder',
    'update_semantictypes',
    'vertex_nomination',
    'vertex_nomination_parser',
    'vertex_nomination_seeded_graph_matching',
    'vgg16',
    'vgg16_image_feature',
    'video_reader',
    'word_2_vec',
    'zero_count'
]
